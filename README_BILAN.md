/!\IMPORTANT/!\
Avant de lancer pour la première fois le projet sur votre ordinateur, créez un sous-dossier vide nommé "storage" dans le dossier "src"
Le .json recensant toutes les games sera enregistré dans <chemin_vers_le_projet>/src/storage

**Bilan du projet rendu :**

- Le jeu est entièrement fonctionnel, toutes les routes ont été faites et testées (manuellement et via Jest)

- Suites à certains soucis avec Jest (et les mocks), certains tests, d'intégration notamment, ont été réalisés plus tard sur une
autre branche (integrationTests) aifn que nous ne perdions pas trop de temps dans le développement et puissions terminer à temps.
Ils n'ont pas été mergé car certains problèmes avec les mocks n'ont pu être réglés, et donc les tests d'intégrations créaient des parties "test", qui ne se supprimaient pas assez rapidement grace à une fonction écrite par nos soins, et étaient donc visible un court instant sur le front. Quelques-uns de ces tests d'intégration sont donc disponibles via la commande *** git checkout integrationTests**

- Sur toutes les parties effectuées jusqu'à présent, aucun bug n'a été détecté IG (In Game), merci de le faire remonter si vous en trouvez un.

- Ce projet a été fait la plupart du temps via Live Share, et donc même si la plupart des commits sont fait par une personne, et des merges par une autre, le projet a bien été fait en binôme.


**Details techniques :**

Le front n'update pour l'instant pas les cartes buildings en construction en fonction des workers qui travaillent déjà dessus (et n'affiche donc pas le nombre de ressources restantes nécessaires afin de finir le building, seulement les ressources totales nécessaires). Il serait possible soit de traiter cela depuis le back (en modifiant directement les propriétés wood, tiles, stone et knowledge dans la carte building lorsqu'on envoie un worker travailler), ou bien depuis le front lorsqu'on récupère le building et les workers travaillant dessus.

Suite aux recommandations du cours et par simplicité, les games sont sauvegardées dans "src/storage/database.json". Cette manière n'est évidemment absolument pas optimale et il serait largement préférable, dans l'optique de stocker une grande quantité de game, d'utiliser une base de donnée.