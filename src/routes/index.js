import cardRouter from "./cardRouter"
import healthRouter from "./healthRouter"
import gamesRouter from "./gamesRouter"
import express from "express"

const router = express.Router()

// Ne pas oubliez d'ajouter le routeur ici
router.use("/cards", cardRouter)
router.use("/health", healthRouter)
router.use("/games", gamesRouter)

export default router
