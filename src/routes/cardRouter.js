import express from "express"
import HttpError from "../middlewares/HttpError"
import * as cardService from "../services/cardService"
const router = express.Router()

// Accessible via http://localhost:3000/cards/workers
router.get("/workers", async function(req, res) {
  try {
    const workers = await cardService.importWorkers()
    res.json(workers)
  } catch (e) {
    // Retourne une erreur 500 si la lecture à échouée
    throw new HttpError(500, "Can't read workers cards.")
  }
})

// Accessible via http://localhost:3000/cards/buildings
router.get("/buildings", async function(req, res) {
  try {
    const buildings = await cardService.importBuildings()
    res.json(buildings)
  } catch (e) {
    // Retourne une erreur 500 si la lecture à échouée
    throw new HttpError(500, "Can't read buildings cards.")
  }
})

export default router
