import express from "express"
import HttpError from "../middlewares/HttpError"
import * as gamesService from "../services/gamesService"
import * as actionsService from "../services/actionsService"
const router = express.Router()

// POST : http://localhost:3000/games/ */
router.post("/", async function(req, res) {
  const numberOfPlayers = req.body.numberOfPlayers
  const name = req.body.name
  const doshuffle = req.body.shuffle
  try {
    gamesService.checkParams(numberOfPlayers, name)
    const newGame = await gamesService.createGame(
      numberOfPlayers,
      name,
      doshuffle
    )
    await gamesService.saveGameToDatabase(newGame)
    res.json(gamesService.filterPrivateField(newGame))
  } catch (e) {
    // Retourne une erreur 500 si la lecture à échouée
    throw new HttpError(500, "Failed to create a new game")
  }
})

// Accessible via http://localhost:3000/games/ */
router.get("/", async function(req, res) {
  try {
    const allGames = await gamesService.findAllGames()
    res.json(allGames)
  } catch (e) {
    // Retourne une erreur 500 si la lecture à échouée
    throw new HttpError(500, "Can't read buildings cards.")
  }
})

// Accessible via http://localhost:3000/games/{gameId} */
router.get("/:gameId", async function(req, res) {
  try {
    const gameId = req.params.gameId
    const game = await gamesService.getGame(gameId)
    res.json(gamesService.filterPrivateField(game))
  } catch (e) {
    throw new HttpError(404, "Game not found")
  }
})

router.post("/:gameId/actions", async function(req, res) {
  let turnflag = false // flag indiquant si c'est au bon joueur de jouer
  try {
    const playerId = parseInt(req.headers["player-id"])
    const gameId = req.params.gameId
    let game = await gamesService.getGame(gameId)
    if (playerId === game.currentPlayer) {
      // On appelle chaque action en fonction de la requête, puis on update dans database.json
      switch (req.body.type) {
        case "TAKE_BUILDING":
          game = actionsService.takeBuilding(
            game,
            req.body.payload.buildingId,
            playerId
          )
          break
        case "TAKE_WORKER":
          game = actionsService.takeWorker(
            game,
            req.body.payload.workerId,
            playerId
          )
          break
        case "TAKE_MONEY":
          game = actionsService.takeMoney(
            game,
            req.body.payload.numberOfActions,
            playerId
          )
          break
        case "SEND_WORKER":
          game = actionsService.sendWorker(
            game,
            playerId,
            req.body.payload.workerId,
            req.body.payload.buildingId
          )
          break
        case "BUY_ACTION":
          game = actionsService.buyAction(game, playerId)
          break
        case "END_TURN":
          game = actionsService.endTurn(game, playerId)
          break
        default:
          throw Error()
      }
      gamesService.updateGame(game)
      res.json(game)
    } else {
      // Si le mauvais joueur a tenté de jouer
      turnflag = true
      throw new Error()
    }
  } catch (e) {
    if (turnflag) throw new HttpError(401, "It's not the player turn.")
    throw new HttpError(400, "Can't execute this action.")
  }
})

export default router
