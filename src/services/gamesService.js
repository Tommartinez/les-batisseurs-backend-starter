import { v4 as uuidv4 } from "uuid"
import { importBuildings, importWorkers } from "./cardService"
import fs from "fs"
import path from "path"

export function createPlayer(id, ouvrier) {
  let act = 0
  if (id === 1) {
    act = 3
  }
  const player = {
    id: id,
    finishedBuildings: [],
    availableWorkers: [ouvrier],
    underConstructionBuildings: [],
    money: 10,
    victoryPoints: 0,
    actions: act
  }
  return player
}

export function getApprenti(ouvriers, nbPlayers, doshuffle) {
  let apprentisList = ouvriers.slice(0, 5)
  if (doshuffle) {
    const _ = require("lodash")
    apprentisList = _.shuffle(apprentisList)
  }
  const res = apprentisList.slice(0, nbPlayers)
  const newWorkers = apprentisList.slice(nbPlayers)
  ouvriers = ouvriers.slice(6).concat(newWorkers)
  const tmp = { res: res, workers: ouvriers }
  return tmp
}

export async function createGame(numberOfPlayers, name, doshuffle = true) {
  const playerlist = []
  const workers = await importWorkers()
  let allBuildings = await importBuildings()
  const tmp = getApprenti(workers, numberOfPlayers, doshuffle)
  const starterApprentis = tmp.res
  let allWorkers = tmp.workers
  for (let i = 0; i < numberOfPlayers; i++) {
    playerlist[i] = createPlayer(i + 1, starterApprentis[i])
  }
  if (doshuffle) {
    // si le bool shuffle est à true alors on mélange les decks.
    const _ = require("lodash")
    allWorkers = _.shuffle(allWorkers)
    allBuildings = _.shuffle(allBuildings)
  }
  const game = {
    id: uuidv4(),
    currentPlayer: 1,
    moneyAvailable: 25 + 5 * 15 - numberOfPlayers * 10,
    workers: allWorkers.slice(0, 5),
    buildings: allBuildings.slice(0, 5),
    remainingWorkers: 37 - numberOfPlayers,
    remainingBuildings: 37,
    nextWorker: allWorkers[5],
    nextBuilding: allBuildings[5],
    done: false,
    lastTurn: false,
    name: name,
    createdDate: new Date(),
    players: playerlist,
    _private: {
      workersDeck: allWorkers.slice(6),
      buildingsDeck: allBuildings.slice(6)
    }
  }
  return game
}

export function checkParams(numberOfPlayers, name) {
  if (numberOfPlayers < 2 || numberOfPlayers > 4 || name === "") {
    throw new Error("Le nombre de joueur doit être compris entre 2 et 4 !")
  }
}

export function filterPrivateField(obj) {
  delete obj._private
  return obj
}

export async function saveGameToDatabase(game) {
  const gamesPath = path.join(__dirname, "../storage/database.json")
  try {
    const gamesFile = await fs.promises.readFile(gamesPath)
    const tabgames = JSON.parse(gamesFile)
    tabgames.push(game)
    await fs.promises.writeFile(gamesPath, JSON.stringify(tabgames))
  } catch (e) {
    // On arrive ici ssi aucune game a été enregistré -> database.json existe ap
    await fs.promises.writeFile(gamesPath, JSON.stringify([game]))
  }
}

export async function findAllGames() {
  const gamesPath = path.join(__dirname, "../storage/database.json")
  let res = []
  try {
    const gamesFile = await fs.promises.readFile(gamesPath)
    const tabgames = JSON.parse(gamesFile)
    // Pour chaque game, on "épure" pour ne garder que les infos importantes
    res = tabgames.map(infosGame)
  } catch (e) {
    // Si database.json existe pas, alors on ne fait rien -> on renvoie un tableau vide
  }
  return res
}

export function infosGame(game) {
  const res = {
    id: game.id,
    name: game.name,
    numberOfPlayers: game.players.length,
    done: game.done,
    createdDate: game.createdDate
  }
  return res
}

export async function getGame(gameId) {
  const gamesPath = path.join(__dirname, "../storage/database.json")
  const gamesFile = await fs.promises.readFile(gamesPath)
  const tabgames = JSON.parse(gamesFile)
  const res = tabgames.find(game => game.id === gameId)
  // L'exception n'est pas catché donc elle va remonter si elle est levée.
  return res
}

export async function updateGame(game) {
  const gamesPath = path.join(__dirname, "../storage/database.json")
  const gamesFile = await fs.promises.readFile(gamesPath)
  const tabgames = JSON.parse(gamesFile)
  const deprecGameId = tabgames.findIndex(g => g.id === game.id)
  tabgames[deprecGameId] = game
  await fs.promises.writeFile(gamesPath, JSON.stringify(tabgames))
}
