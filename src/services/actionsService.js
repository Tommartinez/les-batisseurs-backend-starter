/* A chaque actions appelées ci-dessous, on SAIT QUE
    - l'idPlayer est bien à celui qui doit jouer
*/

// import { forEach } from "lodash"

export function takeBuilding(game, idBuilding, idPlayer) {
  const joueur = game.players[idPlayer - 1]
  if (joueur.actions > 0) {
    const indexBuilding = game.buildings.findIndex(
      building => building.id === idBuilding
    )
    joueur.underConstructionBuildings.push(game.buildings[indexBuilding])
    const index = joueur.underConstructionBuildings.length - 1
    // On rajoute un attribut au building : workers [], qui listera les workers travaillant sur ce building et action needed utile pour le sendWorker
    joueur.underConstructionBuildings[index].workers = []
    joueur.underConstructionBuildings[index].actionsNeeded = 1
    // On remet bien en place le reste du deck pour la suite du jeu buildings - nextBuilding - buildingsDeck
    game.buildings[indexBuilding] = game.nextBuilding
    game.nextBuilding = game._private.buildingsDeck[0]
    game._private.buildingsDeck = game._private.buildingsDeck.slice(1)
    joueur.actions--
  } else throw new Error("Pas assez d'actions restantes")
  return game
}

export function takeWorker(game, idWorker, idPlayer) {
  const joueur = game.players[idPlayer - 1]
  if (joueur.actions > 0) {
    const indexWorker = game.workers.findIndex(worker => worker.id === idWorker)
    joueur.availableWorkers.push(game.workers[indexWorker])
    game.workers[indexWorker] = game.nextWorker
    game.nextWorker = game._private.workersDeck[0]
    game._private.workersDeck = game._private.workersDeck.slice(1)
    joueur.actions--
  } else throw new Error("Pas assez d'actions restantes")
  return game
}

export function takeMoney(game, numberOfActions, idPlayer) {
  const joueur = game.players[idPlayer - 1]
  if (joueur.actions < numberOfActions) {
    throw Error("On triche pas hihihi UwU")
  } else {
    if (numberOfActions === 1 && game.moneyAvailable > 0) {
      joueur.money++
      game.moneyAvailable--
    } else if (numberOfActions === 2 && game.moneyAvailable > 2) {
      joueur.money += 3
      game.moneyAvailable -= 3
    } else if (numberOfActions === 3 && game.moneyAvailable > 5) {
      joueur.money += 6
      game.moneyAvailable -= 6
    } else throw Error("Plus assez d'argent dispo")
    joueur.actions -= numberOfActions
  }
  return game
}

export function buyAction(game, idPlayer) {
  const joueur = game.players[idPlayer - 1]
  if (joueur.money > 4) {
    joueur.money -= 5
    joueur.actions++
  } else throw Error("Pas assez d'argent pour acheter une action en plus :(")
  return game
}

export function endTurn(game, idPlayer) {
  const joueur = game.players[idPlayer - 1]
  if (!game.done) {
    if (isVictoryPoint17OrMore(game, idPlayer)) {
      // vérifie si le joueur qui a finit son tour a 17 points de victoire ou plus
      game.lastTurn = true
    }
    if (game.lastTurn && idPlayer === game.players.length) {
      // Quand le dernier joueur durant le dernier tour termine son tour, on clôt la game
      joueur.actions = 0
      game.done = true
      game.players.forEach(
        player => (player.victoryPoints += ~~(player.money / 10))
      )
    } else {
      joueur.actions = 0
      joueur.underConstructionBuildings.forEach(b => (b.actionsNeeded = 1))
      try {
        game.players[idPlayer].actions = 3
        game.currentPlayer = idPlayer + 1
      } catch {
        // Si out of range -> alors c'est que c'était au joueur d'id max de jouer, le prochain est donc le joueur 1
        game.players[0].actions = 3
        game.currentPlayer = 1
      }
    }
  } else throw Error("La game est finie ! UwU")
  return game
}

export function sendWorker(game, idPlayer, idWorker, idBuilding) {
  const joueur = game.players[idPlayer - 1]
  // On récupère les index des workers et buildings du joueur (throw une erreur si le joueur ne les a pas)
  const indexbuilding = game.players[
    idPlayer - 1
  ].underConstructionBuildings.findIndex(building => building.id === idBuilding)
  const indexworker = joueur.availableWorkers.findIndex(
    worker => worker.id === idWorker
  )
  // On teste si il a assez d'actions ET qu'il a l'argent pour payer l'ouvrier
  if (
    joueur.actions >=
      joueur.underConstructionBuildings[indexbuilding].actionsNeeded &&
    joueur.money >= joueur.availableWorkers[indexworker].price
  ) {
    // On met le worker dans le tableau workers du building spécifié (on envoie le worker travailler)
    joueur.underConstructionBuildings[indexbuilding].workers.push(
      joueur.availableWorkers[indexworker]
    )

    // Si tout bon, on met à jour l'argent et le actionsNeeded du joueur
    joueur.money -= joueur.availableWorkers[indexworker].price
    joueur.actions -= joueur.underConstructionBuildings[indexbuilding]
      .actionsNeeded++

    // On supprime le worker du tableau de availableWorker
    deleteElement(indexworker, joueur.availableWorkers)
    // Teste si le batiment est terminé, et si oui libère tous les workers
    if (isBuildingFinished(joueur.underConstructionBuildings[indexbuilding])) {
      // On rerend tous les workers available
      joueur.underConstructionBuildings[indexbuilding].workers.forEach(w =>
        joueur.availableWorkers.push(w)
      )
      // On met les points de victoire au joueur
      joueur.victoryPoints +=
        joueur.underConstructionBuildings[indexbuilding].victoryPoint
      // Si c'est une machine on crée l'ouvrier
      if (joueur.underConstructionBuildings[indexbuilding].reward === 0)
        game = createMachine(game, idPlayer, indexbuilding)
      // Sinon on donne le reward au joueur
      else
        joueur.money += joueur.underConstructionBuildings[indexbuilding].reward

      // On range le building dans finishedBuildings et on le delete de underConstructionBuildings
      joueur.finishedBuildings.push(
        joueur.underConstructionBuildings[indexbuilding]
      )
      deleteElement(indexbuilding, joueur.underConstructionBuildings)
    }
  } else throw new Error("Plus assez d'actions ou d'argent UwU")
  return game
}

export function deleteElement(index, tab) {
  // On met l'élément à supprimer à la derniere place et on le pop
  if (index !== tab.length - 1) {
    const tmp = tab[index]
    tab[index] = tab[tab.length - 1]
    tab[tab.length - 1] = tmp
  }
  tab.pop()
}

// fonction qui renvoie true si le batiment est terminé et false sinon
export function isBuildingFinished(building) {
  const bilan = { stone: 0, tile: 0, knowledge: 0, wood: 0 }
  // On fait le bilan de toutes les ressources accumulées par les ouvriers sur un chantier
  building.workers.forEach(worker => {
    bilan.stone += worker.stone
    bilan.tile += worker.tile
    bilan.knowledge += worker.knowledge
    bilan.wood += worker.wood
  })
  return (
    building.stone <= bilan.stone &&
    building.wood <= bilan.wood &&
    building.knowledge <= bilan.knowledge &&
    building.tile <= bilan.tile
  )
}

export function createMachine(game, idPlayer, indexbuilding) {
  // A partir d'une batiment-machine qui est construit, on crée un ouvrier-machine correspondant qu'on push dans availableWorkers
  const building =
    game.players[idPlayer - 1].underConstructionBuildings[indexbuilding]
  const tmp = {
    price: 0,
    id: building.id,
    stone: building.stoneProduced,
    knowledge: building.knowledgeProduced,
    tile: building.tileProduced,
    wood: building.woodProduced
  }
  game.players[idPlayer - 1].availableWorkers.push(tmp)

  return game
}

export function isVictoryPoint17OrMore(game, idPlayer) {
  return game.players[idPlayer - 1].victoryPoints >= 17
}
