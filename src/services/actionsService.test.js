import * as actionsService from "./actionsService"
import * as gamesService from "./gamesService"

describe("Takebuilding", () => {
  test("Doit modifier la game et attribuer un building au joueur", async () => {
    const defaultgame = await gamesService.createGame(4, "defaultname", false)
    const testgame = JSON.parse(JSON.stringify(defaultgame))
    const game = actionsService.takeBuilding(
      testgame,
      testgame.buildings[1].id,
      1
    )
    // On vérifie que toutes les modifs sont bien effectuées
    expect(game.players[0].underConstructionBuildings[0].id).toEqual(
      defaultgame.buildings[1].id
    )
    expect(game.buildings[1].id).toEqual(defaultgame.nextBuilding.id)
    expect(game.nextBuilding.id).toEqual(
      defaultgame._private.buildingsDeck[0].id
    )
    expect(game.players[0].actions).toEqual(2)
  })
})

describe("TakeWorker", () => {
  test("Doit modifier la game et attribuer un worker au joueur", async () => {
    const defaultgame = await gamesService.createGame(4, "defaultname", false)
    const testgame = JSON.parse(JSON.stringify(defaultgame))
    const game = actionsService.takeWorker(testgame, testgame.workers[3].id, 1)
    // On vérifie que toutes les modifs sont bien effectuées
    expect(game.players[0].availableWorkers[1].id).toEqual(
      defaultgame.workers[3].id
    )
    expect(game.workers[3].id).toEqual(defaultgame.nextWorker.id)
    expect(game.nextWorker.id).toEqual(defaultgame._private.workersDeck[0].id)
    expect(game.players[0].actions).toEqual(2)
  })
})

describe("TakeMoney", () => {
  test("Doit modifier la game lorsque le joueur désire prendre de l'argent", async () => {
    const defaultgame = await gamesService.createGame(4, "defaultname", false)
    const game = actionsService.takeMoney(defaultgame, 3, 1)
    expect(game.players[0].actions).toEqual(0)
    expect(game.players[0].money).toEqual(16)
  })
})

describe("BuyAction", () => {
  test("Doit modifier la game lorsque le joueur désire acheter une nouvelle action", async () => {
    const defaultgame = await gamesService.createGame(4, "defaultname", false)
    let game = actionsService.buyAction(
      actionsService.buyAction(defaultgame, 1),
      1
    )
    expect(game.players[0].actions).toEqual(5)
    expect(game.players[0].money).toEqual(0)
    try {
      game = expect(actionsService.buyAction(game, 1))
      expect(true).toEqual(false)
    } catch (e) {
      expect(e.message).toBe(
        "Pas assez d'argent pour acheter une action en plus :("
      )
    }
  })
})

describe("EndTurn", () => {
  test("Doit modifier la game lorsque le joueur termine son tour", async () => {
    const defaultgame = await gamesService.createGame(3, "defaultname", false)
    let game = actionsService.endTurn(defaultgame, 1)
    expect(game.players[0].actions).toEqual(0)
    expect(game.players[1].actions).toEqual(3)
    game = actionsService.endTurn(game, 2)
    expect(game.players[2].actions).toEqual(3)
    game = actionsService.endTurn(game, 3)
    expect(game.players[2].actions).toEqual(0)
    expect(game.players[0].actions).toEqual(3)
  })
})

describe("deleteElement", () => {
  test("Doit supprimer un élement du tableau de façon 'clean' :)", async () => {
    const tab = [0, 1, 2, 3, 4, 5]
    actionsService.deleteElement(2, tab)
    expect(tab).toStrictEqual([0, 1, 5, 3, 4])
    actionsService.deleteElement(4, tab)
    expect(tab).toStrictEqual([0, 1, 5, 3])
  })
})

describe("sendWorker ET d'autres actions aussi du coup", () => {
  test("Doit effectuer correctement l'action SEND_WORKER en plus de END_TURN, TAKE_WORKER, TAKE_BUILDING", async () => {
    const defaultgame = await gamesService.createGame(2, "defaultname", false)
    const testgame = JSON.parse(JSON.stringify(defaultgame))
    // Le joueur 1 recrute 1 ouvrier et 1 building pour se préparer à finir un chantier
    let game = actionsService.takeWorker(testgame, testgame.workers[4].id, 1)
    game = actionsService.takeBuilding(game, game.buildings[0].id, 1)
    game = actionsService.endTurn(game, 1)
    game = actionsService.endTurn(game, 2)
    // On envoie les 2 workers pour finir le building
    game = actionsService.sendWorker(
      game,
      1,
      game.players[0].availableWorkers[0].id,
      game.players[0].underConstructionBuildings[0].id
    )
    game = actionsService.sendWorker(
      game,
      1,
      game.players[0].availableWorkers[0].id,
      game.players[0].underConstructionBuildings[0].id
    )
    // Check de la money, victorypoint, ...
    expect(game.players[0].actions).toEqual(0)
    expect(game.players[0].victoryPoints).toEqual(
      defaultgame.buildings[0].victoryPoint
    )
    expect(game.players[0].underConstructionBuildings).toStrictEqual([])
    expect(game.players[0].availableWorkers.length).toBe(3)
    expect(game.players[0].finishedBuildings[0].id).toBe(
      defaultgame.buildings[0].id
    )
    expect(game.players[0].availableWorkers[2].id).toEqual(
      defaultgame.buildings[0].id
    )
  })
})

describe("EndTurn FIN DE GAME", () => {
  test("Doit modifier la game lorsque la partie est terminée", async () => {
    const defaultgame = await gamesService.createGame(3, "defaultname", false)
    expect(defaultgame.lastTurn).toBe(false)
    defaultgame.players[0].victoryPoints = 18
    let game = actionsService.endTurn(defaultgame, 1)
    expect(game.done).toBe(false)
    expect(game.lastTurn).toBe(true)
    game = actionsService.endTurn(game, 2)
    game = actionsService.endTurn(game, 3)
    expect(game.done).toBe(true)

    try {
      game = expect(actionsService.endTurn(game, 1))
      expect(true).toEqual(false)
    } catch (e) {
      expect(e.message).toBe("La game est finie ! UwU")
    }
  })
})
